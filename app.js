const http = require("http");
const express = require("express");
const app = express();
const bodyparser = require("body-parser");

app.set('view engine',"ejs");
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({extended:true}));

let datos = [{

    matricula:"2020030100",
    nombre:"ACOSTA ORTEGA JESUS HUMBERTO",
    sexo:"M",
    materias:["Ingles","Tecnologías de Internet","Base de Datos"]
},
{
    matricula:"2020030220",
    nombre:"ACOSTA VARELA IRVING GUADALUPE",
    sexo:"M",
    materias:["Ingles","Tecnologías de Internet","Base de Datos"]
},
{
    matricula:"2020030300",
    nombre:"ALMOGAR VAZQUEZ YARLEN DE JESUS",
    sexo:"F",
    materias:["Ingles","Tecnologías de Internet","Base de Datos"]
},{
    matricula:"2020030219",
    nombre:"LUNA SANDOVAL RUY JESÉ",
    sexo:"M",
    materias:["Ingles","Tecnologías de Internet","Base de Datos"]
},
{
    matricula:"2020030331",
    nombre:"LOPEZ GONZALES ERICK JEANICK",
    sexo:"M",
    materias:["Ingles","Tecnologías de Internet","Base de Datos"]
},
{
    matricula:"2020030106",
    nombre:"MORENO LOPEZ JUAN CARLOS",
    sexo:"M",
    materias:["Ingles","Tecnologías de Internet","Base de Datos"]
}
]



app.get('/',(req,res)=>{
    
    //    res.send(" <h1>Iniciamos con express</h1>");

    res.render('index',{titulo: "Listado de Alumnos", listado: datos});
    
});

app.get("/tablas",(req,res)=>{
    
    const valores = {
        tablas:req.query.tablas
    }
    res.render('tablas',valores);

})

app.post("/tablas",(req,res)=>{

    const valores = {
        tablas:req.body.tablas
    }

    res.render('tablas',valores)

})

// Cosas de la practica de cotización
app.get("/cotizacion",(req,res)=>{

    const valores = {
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos
    }

    res.render('cotizacion',valores)

})

app.post("/cotizacion",(req,res)=>{

    const valores = {
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        plazos:req.body.plazos
    }

    res.render('cotizacion',valores)

})

app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname+'/public/error.html')
})
const puerto = 3002;
app.listen(puerto,()=>{
    console.log("Iniciado en puerto 3000.");
});